import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.BorderLayout;
import java.awt.Desktop;
import javax.swing.JTextArea;

public class Stats {

	private JFrame frmMecanografia;

	static Nivel nivel = new Nivel();
	Estadisticas estadisticas = new Estadisticas();
	
	public JFrame getFrame() {
		return frmMecanografia;
	}

	public void setFrame(JFrame frame) {
		this.frmMecanografia = frame;
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Stats window = new Stats();
					window.frmMecanografia.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	

	/**
	 * Create the application.
	 */
	public Stats() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMecanografia = new JFrame();
		frmMecanografia.setTitle("Mecanografia");
		frmMecanografia.setResizable(false);
		frmMecanografia.setIconImage(Toolkit.getDefaultToolkit().getImage("imagenes\\icono.jpg"));
		frmMecanografia.setBounds(100, 100, 450, 300);
		frmMecanografia.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMecanografia.getContentPane().setLayout(null);
		
		JButton btnNivel = new JButton("Seleccionar Nivel");
		btnNivel.setBounds(150, 50, 150, 50);
		frmMecanografia.getContentPane().add(btnNivel);
		
		btnNivel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

						nivel.getFrame().setVisible(true);
						frmMecanografia.setVisible(false);
			}
		});
		
		JButton btnEstadisticas = new JButton("Estadisticas");
		btnEstadisticas.setBounds(150, 150, 150, 50);
		frmMecanografia.getContentPane().add(btnEstadisticas);
		
		btnEstadisticas.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				estadisticas.getFrame().setVisible(true);
				frmMecanografia.setVisible(false);
				
			}
		});
		
		JButton btnVolver = new JButton("VOLVER");
		btnVolver.setBounds(335, 227, 89, 23);
		frmMecanografia.getContentPane().add(btnVolver);
		
		btnVolver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				Carga.login1.getFrame().setVisible(true);
				frmMecanografia.setVisible(false);
			}
		});
	}
}
