import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.management.timer.Timer;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;

import java.util.TimerTask;
import java.awt.Toolkit;


public class Principal {
	
	
	public Fichero f = new Fichero("archivos\\leccion.txt");
	
	static int cnt = 0;
	
	private JFrame frame;
	public static JTextPane textPane = new JTextPane();
	static JTextPane tiempo;
	private static int contador;
	static char[] textoSen;
	static char[] textoCom;
	static int errores = 0;
	
	static JButton btnIniciar;
	static Temporizador tmp = new Temporizador();
	static JTextPane PulTotales;
	static JTextPane PulMinuto;
	static JTextPane Errores;
	
	public JFrame getFrame() {
		return frame;
	}

	static int[] cFallos = new int[500];
	
	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	static DefaultHighlighter.DefaultHighlightPainter highlightPainterT = new DefaultHighlighter.DefaultHighlightPainter(Color.green);
	static DefaultHighlighter.DefaultHighlightPainter highlightPainterF = new DefaultHighlighter.DefaultHighlightPainter(Color.red);
	
	static int pulsaciones = 0;
	static char car;
	static int veces = 0;
	
	static JButton btnZ = new JButton("Z");
	static JButton btnX = new JButton("X");
	static JButton btnC = new JButton("C");
	static JButton btnV = new JButton("V");
	static JButton btnB = new JButton("B");
	static JButton btnN = new JButton("N");
	static JButton btnM = new JButton("M");
	static JButton btnA = new JButton("A");
	static JButton btnS = new JButton("S");
	static JButton btnD = new JButton("D");
	static JButton btnF = new JButton("F");
	static JButton btnH = new JButton("H");
	static JButton btnJ = new JButton("J");
	static JButton btnG = new JButton("G");
	static JButton btnK = new JButton("K");
	static JButton btnL = new JButton("L");
	static JButton btnQ = new JButton("Q");
	static JButton btnW = new JButton("W");
	static JButton btnE = new JButton("E");
	static JButton btnR = new JButton("R");
	static JButton btnT = new JButton("T");
	static JButton btnY = new JButton("Y");
	static JButton btnU = new JButton("U");
	static JButton btnI = new JButton("I");
	static JButton btnO = new JButton("O");
	static JButton btnP = new JButton("P");
	static JButton buttonEspacio = new JButton("");
	static JButton btn1 = new JButton("1");
	static JButton btn2 = new JButton("2");
	static JButton btn3 = new JButton("3");
	static JButton btn4 = new JButton("4");
	static JButton btn5 = new JButton("5");
	static JButton btn6 = new JButton("6");
	static JButton btn7 = new JButton("7");
	static JButton btn8 = new JButton("8");
	static JButton btn9 = new JButton("9");
	static JButton btn0 = new JButton("0");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal window = new Principal();
					window.frame.setVisible(true);
					} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	

	public static void teclado() {
	
		textPane.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(java.awt.event.KeyEvent e) {
				
				btnIniciar.doClick();
				
				pulsaciones++;
				String pul = String.valueOf(pulsaciones); 
				PulTotales.setText(pul);

				String pulMinuto;
				pulMinuto = String.valueOf((60 * pulsaciones)/tmp.getSegundos());
				PulMinuto.setText(pulMinuto);	
				
				String err = String.valueOf(errores); 
				Errores.setText(err);
				
				textoSen = Fichero.textoSencillo.toCharArray();
				if (contador<textoSen.length) {
				car = (char) e.getKeyChar();
				if (textoSen[contador] == car) {
					
					try {
						textPane.getHighlighter().addHighlight(cnt, cnt+1, highlightPainterT);
						cnt++;
					} catch (BadLocationException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					if(e.getKeyCode() == KeyEvent.VK_Q) {
						btnQ.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_W) {
						btnW.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_E) {
						btnE.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_R) {
						btnR.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_T) {
						btnT.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_Y) {
						btnY.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_U) {
						btnU.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_I) {
						btnI.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_O) {
						btnO.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_P) {
						btnP.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_A) {
						btnA.setBackground(Color.green);
					}
					if(e.getKeyCode() == KeyEvent.VK_S) {
						btnS.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_D) {
						btnD.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_F) {
						btnF.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_G) {
						btnG.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_H) {
						btnH.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_J) {
						btnJ.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_K) {
						btnK.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_L) {
						btnL.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_Z) {
						btnZ.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_X) {
						btnX.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_C) {
						btnC.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_V) {
						btnV.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_B) {
						btnB.setBackground(Color.green);
					}
					if(e.getKeyCode() == KeyEvent.VK_N) {
						btnN.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_M) {
						btnM.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_SPACE) {
						buttonEspacio.setBackground(Color.GREEN);
					}
				}
				else {
					try {
						textPane.getHighlighter().addHighlight(cnt, cnt+1, highlightPainterF);
						cnt++;
					} catch (BadLocationException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					errores++;
					if(e.getKeyCode() == KeyEvent.VK_Q) {
						btnQ.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_W) {
						btnW.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_E) {
						btnE.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_R) {
						btnR.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_T) {
						btnT.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_Y) {
						btnY.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_U) {
						btnU.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_I) {
						btnI.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_O) {
						btnO.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_P) {
						btnP.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_A) {
						btnA.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_S) {
						btnS.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_D) {
						btnD.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_F) {
						btnF.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_G) {
						btnG.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_H) {
						btnH.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_J) {
						btnJ.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_K) {
						btnK.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_L) {
						btnL.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_Z) {
						btnZ.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_X) {
						btnX.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_C) {
						btnC.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_V) {
						btnV.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_B) {
						btnB.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_N) {
						btnN.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_M) {
						btnM.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_SPACE) {
						buttonEspacio.setBackground(Color.RED);
					}
				}
				contador++;
				}
				else{
				//estadisticas
					try {
						File fichero = new File("archivos\\stats.txt");
						FileWriter fw = null; 
						fw = new FileWriter(fichero, true);
						
						String user = Login.nombreUsuario;
						
						fw.write(user + pulsaciones + ":");
						fw.write(pulMinuto + ":");
						fw.write(errores + ":");
						fw.write(Temporizador.getSegundos()+ "\n");
						
						fw.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				JOptionPane.showMessageDialog(null, "Programa terminado correctamente, errores: "+errores+" tiempo: "+Temporizador.getSegundos()+ " pulsaciones por minuto: "+pulMinuto	,"ERROR", JOptionPane.WARNING_MESSAGE);
				System.exit(0);
				}
			}
		
			public void keyReleased(java.awt.event.KeyEvent arg0) {		//Actua al soltar la tecla
				btnQ.setBackground(Color.white);
				btnW.setBackground(Color.white);
				btnE.setBackground(Color.white);
				btnR.setBackground(Color.white);
				btnT.setBackground(Color.white);
				btnY.setBackground(Color.white);
				btnU.setBackground(Color.white);
				btnI.setBackground(Color.white);
				btnO.setBackground(Color.white);
				btnP.setBackground(Color.white);
				btnA.setBackground(Color.white);
				btnS.setBackground(Color.white);
				btnD.setBackground(Color.white);
				btnF.setBackground(Color.white);
				btnG.setBackground(Color.white);
				btnH.setBackground(Color.white);
				btnJ.setBackground(Color.white);
				btnK.setBackground(Color.white);
				btnL.setBackground(Color.white);
				buttonEspacio.setBackground(Color.white);
				btnZ.setBackground(Color.white);
				btnX.setBackground(Color.white);
				btnC.setBackground(Color.white);
				btnV.setBackground(Color.white);
				btnB.setBackground(Color.white);
				btnN.setBackground(Color.white);
				btnM.setBackground(Color.white);
			}
		});
	}
	
	public static void teclado2() {
		
		textPane.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(java.awt.event.KeyEvent e) {
				
				btnIniciar.doClick();
				
				pulsaciones++;
				String pul = String.valueOf(pulsaciones); 
				PulTotales.setText(pul);

				String pulMinuto;
				pulMinuto = String.valueOf((60 * pulsaciones)/tmp.getSegundos());
				PulMinuto.setText(pulMinuto);	
				
				String err = String.valueOf(errores); 
				Errores.setText(err);
				
				textoCom = Fichero.textoComplejo.toCharArray();
				if (contador<textoCom.length) {
				car = (char) e.getKeyChar();
				System.out.println(car);
				if (textoCom[contador] == car) {
					try {
						textPane.getHighlighter().addHighlight(cnt, cnt+1, highlightPainterT);
						cnt++;
					} catch (BadLocationException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					if(e.getKeyCode() == KeyEvent.VK_Q) {
						btnQ.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_W) {
						btnW.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_E) {
						btnE.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_R) {
						btnR.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_T) {
						btnT.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_Y) {
						btnY.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_U) {
						btnU.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_I) {
						btnI.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_O) {
						btnO.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_P) {
						btnP.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_A) {
						btnA.setBackground(Color.green);
					}
					if(e.getKeyCode() == KeyEvent.VK_S) {
						btnS.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_D) {
						btnD.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_F) {
						btnF.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_G) {
						btnG.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_H) {
						btnH.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_J) {
						btnJ.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_K) {
						btnK.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_L) {
						btnL.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_Z) {
						btnZ.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_X) {
						btnX.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_C) {
						btnC.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_V) {
						btnV.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_B) {
						btnB.setBackground(Color.green);
					}
					if(e.getKeyCode() == KeyEvent.VK_N) {
						btnN.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_M) {
						btnM.setBackground(Color.GREEN);
					}
					if(e.getKeyCode() == KeyEvent.VK_SPACE) {
						buttonEspacio.setBackground(Color.GREEN);
					}
				}
				else {
					try {
						textPane.getHighlighter().addHighlight(cnt, cnt+1, highlightPainterF);
						cnt++;
					} catch (BadLocationException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					errores++;
					if(e.getKeyCode() == KeyEvent.VK_Q) {
						btnQ.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_W) {
						btnW.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_E) {
						btnE.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_R) {
						btnR.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_T) {
						btnT.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_Y) {
						btnY.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_U) {
						btnU.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_I) {
						btnI.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_O) {
						btnO.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_P) {
						btnP.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_A) {
						btnA.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_S) {
						btnS.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_D) {
						btnD.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_F) {
						btnF.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_G) {
						btnG.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_H) {
						btnH.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_J) {
						btnJ.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_K) {
						btnK.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_L) {
						btnL.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_Z) {
						btnZ.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_X) {
						btnX.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_C) {
						btnC.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_V) {
						btnV.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_B) {
						btnB.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_N) {
						btnN.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_M) {
						btnM.setBackground(Color.RED);
					}
					if(e.getKeyCode() == KeyEvent.VK_SPACE) {
						buttonEspacio.setBackground(Color.RED);
					}
				}
				contador++;
				}
				else{
				//estadisticas
					try {
						File fichero = new File("archivos\\stats.txt");
						FileWriter fw = new FileWriter(fichero);
						
						String user = Login.nombreUsuario;
						
						fw.write(user + pulsaciones + ":");
						fw.write(pulMinuto + ":");
						fw.write(errores + ":");
						fw.write(Temporizador.getSegundos()+ "\n");
						fw.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				System.exit(0);
				}
			}
		
			public void keyReleased(java.awt.event.KeyEvent arg0) {		//Actua al soltar la tecla
				btnQ.setBackground(Color.white);
				btnW.setBackground(Color.white);
				btnE.setBackground(Color.white);
				btnR.setBackground(Color.white);
				btnT.setBackground(Color.white);
				btnY.setBackground(Color.white);
				btnU.setBackground(Color.white);
				btnI.setBackground(Color.white);
				btnO.setBackground(Color.white);
				btnP.setBackground(Color.white);
				btnA.setBackground(Color.white);
				btnS.setBackground(Color.white);
				btnD.setBackground(Color.white);
				btnF.setBackground(Color.white);
				btnG.setBackground(Color.white);
				btnH.setBackground(Color.white);
				btnJ.setBackground(Color.white);
				btnK.setBackground(Color.white);
				btnL.setBackground(Color.white);
				buttonEspacio.setBackground(Color.white);
				btnZ.setBackground(Color.white);
				btnX.setBackground(Color.white);
				btnC.setBackground(Color.white);
				btnV.setBackground(Color.white);
				btnB.setBackground(Color.white);
				btnN.setBackground(Color.white);
				btnM.setBackground(Color.white);
			}
		});
	}
	
	public static void correTiempo() {
		
		String cnt;
		cnt = String.valueOf(tmp.getSegundos());
		tiempo.setText(cnt);
	}
	
	/**
	 * Create the application.
	 */
	public Principal() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage("imagenes\\icono.jpg"));
		frame.setResizable(false);
		frame.getContentPane().setEnabled(false);
		frame.getContentPane().setFont(new Font("Tahoma", Font.PLAIN, 10));
		frame.setBounds(100, 100, 1400, 800);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnNewButton = new JButton("<");
		btnNewButton.setEnabled(false);
		btnNewButton.setBounds(149, 553, 60, 60);
		frame.getContentPane().add(btnNewButton);
		btnZ.setIcon(null);
		btnZ.setBackground(Color.WHITE);
		btnZ.setBounds(219, 553, 60, 60);
		frame.getContentPane().add(btnZ);
		btnX.setIcon(null);
		btnX.setBackground(Color.WHITE);
		btnX.setBounds(289, 553, 60, 60);
		frame.getContentPane().add(btnX);
		btnC.setIcon(null);
		btnC.setBackground(Color.WHITE);
		btnC.setBounds(359, 553, 60, 60);
		frame.getContentPane().add(btnC);
		btnV.setIcon(null);
		btnV.setBackground(Color.WHITE);
		btnV.setBounds(429, 553, 60, 60);
		frame.getContentPane().add(btnV);
		btnB.setIcon(null);
		btnB.setBackground(Color.WHITE);
		btnB.setBounds(499, 553, 60, 60);
		frame.getContentPane().add(btnB);
		btnN.setIcon(null);
		btnN.setBackground(Color.WHITE);
		btnN.setBounds(569, 553, 60, 60);
		frame.getContentPane().add(btnN);
		btnM.setIcon(null);
		btnM.setBackground(Color.WHITE);
		btnM.setBounds(639, 553, 60, 60);
		frame.getContentPane().add(btnM);
		
		JButton button_6 = new JButton(",");
		button_6.setEnabled(false);
		button_6.setBounds(709, 553, 60, 60);
		frame.getContentPane().add(button_6);
		
		JButton button_7 = new JButton(".");
		button_7.setEnabled(false);
		button_7.setBounds(779, 553, 60, 60);
		frame.getContentPane().add(button_7);
		
		JButton button_8 = new JButton("-");
		button_8.setEnabled(false);
		button_8.setBounds(849, 553, 60, 60);
		frame.getContentPane().add(button_8);
		
		JButton button_9 = new JButton("");
		button_9.setEnabled(false);
		button_9.setBounds(919, 553, 170, 60);
		frame.getContentPane().add(button_9);
		
		btnA = new JButton("A");
		btnA.setIcon(null);
		btnA.setBackground(Color.WHITE);
		btnA.setBounds(167, 482, 60, 60);
		frame.getContentPane().add(btnA);
		
		btnS = new JButton("S");
		btnS.setIcon(null);
		btnS.setBackground(Color.WHITE);
		btnS.setBounds(237, 482, 60, 60);
		frame.getContentPane().add(btnS);
		
		btnD = new JButton("D");
		btnD.setIcon(null);
		btnD.setBackground(Color.WHITE);
		btnD.setBounds(307, 482, 60, 60);
		frame.getContentPane().add(btnD);
		
		btnF = new JButton("F");
		btnF.setIcon(null);
		btnF.setBackground(Color.WHITE);
		btnF.setBounds(377, 482, 60, 60);
		frame.getContentPane().add(btnF);
		
		btnG = new JButton("G");
		btnG.setIcon(null);
		btnG.setBackground(Color.WHITE);
		btnG.setBounds(447, 482, 60, 60);
		frame.getContentPane().add(btnG);
		
		btnH = new JButton("H");
		btnH.setIcon(null);
		btnH.setBackground(Color.WHITE);
		btnH.setBounds(517, 482, 60, 60);
		frame.getContentPane().add(btnH);
		
		btnJ = new JButton("J");
		btnJ.setIcon(null);
		btnJ.setBackground(Color.WHITE);
		btnJ.setBounds(587, 482, 60, 60);
		frame.getContentPane().add(btnJ);
		
		btnK = new JButton("K");
		btnK.setIcon(null);
		btnK.setBackground(Color.WHITE);
		btnK.setBounds(657, 482, 60, 60);
		frame.getContentPane().add(btnK);
		
		btnL = new JButton("L");
		btnL.setIcon(null);
		btnL.setBackground(Color.WHITE);
		btnL.setBounds(727, 482, 60, 60);
		frame.getContentPane().add(btnL);
		
		JButton button_19 = new JButton("\u00D1");
		button_19.setEnabled(false);
		button_19.setBounds(797, 482, 60, 60);
		frame.getContentPane().add(button_19);
		
		JButton button_10 = new JButton("\u00B4");
		button_10.setEnabled(false);
		button_10.setBounds(867, 482, 60, 60);
		frame.getContentPane().add(button_10);
		
		JButton button_18 = new JButton("\u00C7");
		button_18.setEnabled(false);
		button_18.setBounds(937, 482, 60, 60);
		frame.getContentPane().add(button_18);
		
		JButton button_20 = new JButton("");
		button_20.setEnabled(false);
		button_20.setBounds(1007, 411, 82, 131);
		frame.getContentPane().add(button_20);
		btnQ.setIcon(null);
		btnQ.setBackground(Color.WHITE);
		
		btnQ.setBounds(149, 411, 60, 60);
		frame.getContentPane().add(btnQ);
		btnW.setIcon(null);
		btnW.setBackground(Color.WHITE);
		
		btnW.setBounds(219, 411, 60, 60);
		frame.getContentPane().add(btnW);
		btnE.setIcon(null);
		btnE.setBackground(Color.WHITE);
		btnE.setBounds(289, 411, 60, 60);
		frame.getContentPane().add(btnE);
		btnR.setIcon(null);
		btnR.setBackground(Color.WHITE);
		btnR.setBounds(359, 411, 60, 60);
		frame.getContentPane().add(btnR);
		btnT.setIcon(null);
		btnT.setBackground(Color.WHITE);
		btnT.setBounds(429, 411, 60, 60);
		frame.getContentPane().add(btnT);
		btnY.setIcon(null);
		btnY.setBackground(Color.WHITE);
		btnY.setBounds(499, 411, 60, 60);
		frame.getContentPane().add(btnY);
		btnU.setIcon(null);
		btnU.setBackground(Color.WHITE);
		btnU.setBounds(569, 411, 60, 60);
		frame.getContentPane().add(btnU);
		btnI.setIcon(null);
		btnI.setBackground(Color.WHITE);
		btnI.setBounds(639, 411, 60, 60);
		frame.getContentPane().add(btnI);
		btnO.setIcon(null);
		btnO.setBackground(Color.WHITE);
		btnO.setBounds(709, 411, 60, 60);
		frame.getContentPane().add(btnO);
		btnP.setIcon(null);
		btnP.setBackground(Color.WHITE);
		btnP.setBounds(779, 411, 60, 60);
		frame.getContentPane().add(btnP);
		
		JButton btnP_1 = new JButton("`");
		btnP_1.setEnabled(false);
		btnP_1.setBounds(849, 411, 60, 60);
		frame.getContentPane().add(btnP_1);
		
		JButton button_32 = new JButton("+");
		button_32.setEnabled(false);
		button_32.setBounds(919, 411, 60, 60);
		frame.getContentPane().add(button_32);
		
		JButton btnCtrl = new JButton("");
		btnCtrl.setEnabled(false);
		btnCtrl.setBounds(57, 553, 82, 60);
		frame.getContentPane().add(btnCtrl);
		
		JButton btnMayus = new JButton("Mayus");
		btnMayus.setEnabled(false);
		btnMayus.setBounds(57, 482, 100, 60);
		frame.getContentPane().add(btnMayus);
		
		JButton btnTab = new JButton("TAB");
		btnTab.setEnabled(false);
		btnTab.setBounds(57, 411, 82, 60);
		frame.getContentPane().add(btnTab);
		
		JButton button_22 = new JButton("\u00BA");
		button_22.setEnabled(false);
		button_22.setBounds(57, 340, 62, 60);
		frame.getContentPane().add(button_22);
		
		JButton button_23 = new JButton("1");
		button_23.setBounds(129, 340, 60, 60);
		frame.getContentPane().add(button_23);
		
		JButton button_24 = new JButton("2");
		button_24.setBounds(199, 340, 60, 60);
		frame.getContentPane().add(button_24);
		
		JButton button_25 = new JButton("3");
		button_25.setBounds(269, 340, 60, 60);
		frame.getContentPane().add(button_25);
		
		JButton button_26 = new JButton("4");
		button_26.setBounds(339, 340, 60, 60);
		frame.getContentPane().add(button_26);
		
		JButton button_27 = new JButton("5");
		button_27.setBounds(409, 340, 60, 60);
		frame.getContentPane().add(button_27);
		
		JButton button_28 = new JButton("6");
		button_28.setBounds(479, 340, 60, 60);
		frame.getContentPane().add(button_28);
		
		JButton button_29 = new JButton("7");
		button_29.setBounds(549, 340, 60, 60);
		frame.getContentPane().add(button_29);
		
		JButton button_30 = new JButton("8");
		button_30.setBounds(619, 340, 60, 60);
		frame.getContentPane().add(button_30);
		
		JButton button_31 = new JButton("9");
		button_31.setBounds(689, 340, 60, 60);
		frame.getContentPane().add(button_31);
		
		JButton button_33 = new JButton("0");
		button_33.setBounds(759, 340, 60, 60);
		frame.getContentPane().add(button_33);
		
		JButton button_34 = new JButton("'");
		button_34.setEnabled(false);
		button_34.setBounds(829, 340, 60, 60);
		frame.getContentPane().add(button_34);
		
		JButton button_35 = new JButton("\u00A1");
		button_35.setEnabled(false);
		button_35.setBounds(899, 340, 60, 60);
		frame.getContentPane().add(button_35);
		
		JButton button_36 = new JButton("<--");
		button_36.setEnabled(false);
		button_36.setBounds(969, 340, 120, 60);
		frame.getContentPane().add(button_36);
		
		JButton btnCtrl_1 = new JButton("Ctrl");
		btnCtrl_1.setEnabled(false);
		btnCtrl_1.setBounds(57, 624, 100, 60);
		frame.getContentPane().add(btnCtrl_1);
		
		JButton button_21 = new JButton("");
		button_21.setEnabled(false);
		button_21.setBounds(167, 624, 60, 60);
		frame.getContentPane().add(button_21);
		
		JButton btnAlt = new JButton("Alt");
		btnAlt.setEnabled(false);
		btnAlt.setBounds(237, 624, 71, 60);
		frame.getContentPane().add(btnAlt);
		buttonEspacio.setIcon(null);
		buttonEspacio.setBackground(Color.WHITE);
		
		
		buttonEspacio.setBounds(318, 624, 399, 60);
		frame.getContentPane().add(buttonEspacio);
		
		JButton btnAltGr = new JButton("Alt Gr");
		btnAltGr.setEnabled(false);
		btnAltGr.setBounds(727, 624, 92, 60);
		frame.getContentPane().add(btnAltGr);
		
		JButton button_1 = new JButton("");
		button_1.setEnabled(false);
		button_1.setBounds(827, 624, 82, 60);
		frame.getContentPane().add(button_1);
		
		JButton button_2 = new JButton("");
		button_2.setEnabled(false);
		button_2.setBounds(919, 624, 71, 60);
		frame.getContentPane().add(button_2);
		
		JButton btnCtrl_2 = new JButton("Ctrl");
		btnCtrl_2.setEnabled(false);
		btnCtrl_2.setBounds(997, 624, 92, 60);
		frame.getContentPane().add(btnCtrl_2);
		
		JLabel lblPulsacionesTotales = new JLabel("Pulsaciones Totales");
		lblPulsacionesTotales.setBounds(1154, 351, 175, 14);
		frame.getContentPane().add(lblPulsacionesTotales);
		
		JLabel lblPulsacionesminuto = new JLabel("Pulsaciones/Minuto");
		lblPulsacionesminuto.setBounds(1154, 434, 175, 14);
		frame.getContentPane().add(lblPulsacionesminuto);
		
		JLabel lblErrores = new JLabel("Errores");
		lblErrores.setBounds(1154, 505, 175, 14);
		frame.getContentPane().add(lblErrores);
		
		JLabel lblTiempo = new JLabel("Tiempo");
		lblTiempo.setBounds(1154, 586, 175, 14);
		frame.getContentPane().add(lblTiempo);
		
		
		textPane.setEditable(false);
		textPane.setFont(new Font("Leelawadee UI Semilight", Font.PLAIN, 22));
		textPane.setBounds(57, 46, 1032, 256);
		frame.getContentPane().add(textPane);
		

		tiempo = new JTextPane();
		tiempo.setBounds(1154, 611, 100, 27);
		frame.getContentPane().add(tiempo);
		tiempo.setEditable(false);
	
		
		btnIniciar = new JButton("INICIAR");
		btnIniciar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnIniciar.setEnabled(false);
				tmp.Contar();
			}
		});
		btnIniciar.setBounds(1168, 114, 161, 52);
		frame.getContentPane().add(btnIniciar);
		
		JButton btnVolver = new JButton("VOLVER");
		btnVolver.setEnabled(false);
		btnVolver.setBounds(1168, 176, 161, 52);
		frame.getContentPane().add(btnVolver);
		
		btnVolver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				Stats.nivel.getFrame().setVisible(true);
				frame.setVisible(false);
			}
		});
		
		PulTotales = new JTextPane();
		PulTotales.setEditable(false);
		PulTotales.setBounds(1154, 373, 100, 27);
		frame.getContentPane().add(PulTotales);	
		
		PulMinuto = new JTextPane();
		PulMinuto.setEditable(false);
		PulMinuto.setBounds(1154, 459, 100, 27);
		frame.getContentPane().add(PulMinuto);
		
		Errores = new JTextPane();
		Errores.setEditable(false);
		Errores.setBounds(1154, 532, 100, 27);
		frame.getContentPane().add(Errores);		
	}
}
