import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.JOptionPane;

public class Fichero {
	
	//guardamos el path del fichero en la variable ruta
    //creamos un objeto File asociado al fichero seleccionado
	String ruta; //"archivos\\leccion.txt"
    File f;
    static int nivel;
    
	Scanner entrada = null;
    String linea;
    int numeroDeLinea = 1;
    boolean contiene = false;
    
    public Fichero (String ruta) {
        this.ruta = ruta;
        f = new File(ruta);
    }
    
    public static int getNivel() {
		return nivel;
	}
    public void setNivel(int nivel) {
		this.nivel = nivel;
	}

    static String textoComplejo;
    static String textoSencillo;
    


	public String cojeTexto() {
    	try {
            //creamos un Scanner para leer el fichero
            entrada = new Scanner(f);
            
            while (entrada.hasNext()) { //mientras no se llegue al final del fichero
                linea = entrada.nextLine();  //se lee una l�nea
               
                int posSeparador = linea.indexOf("*");
            	textoComplejo = linea.substring(posSeparador+1);
            	textoSencillo = linea.substring(0, posSeparador);
                
                if(nivel == 1) {
            		return textoComplejo;
            	}
            	else if(nivel == 0){
            		return textoSencillo;
            	}
                
                
               // System.out.println(textoComplejo);
                
                    contiene = true;
                numeroDeLinea++; //se incrementa el contador de l�neas
            }
            if(!contiene){ //si el archivo no contiene el texto se muestra un mensaje indic�ndolo

            	JOptionPane.showMessageDialog(null, "Usuario o contrase�a incorrecto","ERROR", JOptionPane.WARNING_MESSAGE);
            }
        } catch (FileNotFoundException e) {
            System.out.println(e.toString());
        } catch (NullPointerException e) {
            System.out.println(e.toString() + "No ha seleccionado ning�n archivo");
        } catch (Exception e) {
            System.out.println(e.toString());
        } finally {
            if (entrada != null) {
                entrada.close();
            }
        }
    	return "NO leido";
    }
    
    
	
	

}
