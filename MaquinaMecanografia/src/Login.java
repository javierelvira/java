import java.awt.EventQueue;
import java.awt.Toolkit;
import java.io.*;


import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Scanner;

public class Login {

	private JFrame frmMecanografia;
	public JFrame getFrame() {
		return frmMecanografia;
	}

	public void setFrame(JFrame frame) {
		this.frmMecanografia = frame;
	}

	static String nombreUsuario;
	private JTextField usuario;
	private JPasswordField password;
	public static Stats stats = new Stats();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login window = new Login();
					window.frmMecanografia.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	

	/**
	 * Create the application.
	 */
	
	public void compruebaUsuario() {
		
		Scanner entrada = null;
        String linea;
        int numeroDeLinea = 1;
        boolean contiene = false;
        Scanner sc = new Scanner(System.in);
        nombreUsuario = usuario.getText();
        String passwordUsuario = password.getText();
        
        try {
            //guardamos el path del fichero en la variable ruta
            String ruta = ("archivos\\usuarios.txt");
            //creamos un objeto File asociado al fichero seleccionado
            File f = new File(ruta);
            //creamos un Scanner para leer el fichero
            entrada = new Scanner(f);
            
            while (entrada.hasNext()) { //mientras no se llegue al final del fichero
                linea = entrada.nextLine();  //se lee una l�nea
                int posSeparador = linea.indexOf("*");
                String passwdUsu = linea.substring(posSeparador+1);
                String nombreUsu = linea.substring(0, posSeparador);
                if ( (passwdUsu.contentEquals(passwordUsuario)) && (nombreUsu.contentEquals(nombreUsuario)) ) {   //si la l�nea contiene el texto buscado se muestra por pantalla
                	stats.getFrame().setVisible(true);
					frmMecanografia.setVisible(false);
                    contiene = true;
                }
                numeroDeLinea++; //se incrementa el contador de l�neas
            }
            if(!contiene){ //si el archivo no contiene el texto se muestra un mensaje indic�ndolo

            	JOptionPane.showMessageDialog(null, "Usuario o contrase�a incorrecto","ERROR", JOptionPane.WARNING_MESSAGE);
            }
        } catch (FileNotFoundException e) {
            System.out.println(e.toString());
        } catch (NullPointerException e) {
            System.out.println(e.toString() + "No ha seleccionado ning�n archivo");
        } catch (Exception e) {
            System.out.println(e.toString());
        } finally {
            if (entrada != null) {
                entrada.close();
            }
        }
	}
	
		
	public Login() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
   
	      
		frmMecanografia = new JFrame();
		frmMecanografia.setTitle("Mecanografia");
		frmMecanografia.setIconImage(Toolkit.getDefaultToolkit().getImage("imagenes\\icono.jpg"));
		frmMecanografia.setResizable(false);
		frmMecanografia.setBounds(100, 100, 450, 300);
		frmMecanografia.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMecanografia.getContentPane().setLayout(null);
		
		JLabel NombreUsuario = new JLabel("Nombre de Usuario:");
		NombreUsuario.setBounds(53, 59, 139, 14);
		frmMecanografia.getContentPane().add(NombreUsuario);
		
		usuario = new JTextField();
		usuario.setBounds(180, 59, 118, 20);
		frmMecanografia.getContentPane().add(usuario);
		usuario.setColumns(10);
		
		password = new JPasswordField();
		password.setBounds(180, 134, 118, 20);
		frmMecanografia.getContentPane().add(password);
		
		JLabel lblPassword = new JLabel("Contrase\u00F1a:");
		lblPassword.setBounds(53, 137, 139, 14);
		frmMecanografia.getContentPane().add(lblPassword);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.setBounds(180, 204, 118, 20);
		frmMecanografia.getContentPane().add(btnLogin);
		
		btnLogin.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				compruebaUsuario();
		}
		});		
		
		JButton btnAcercaDe = new JButton("ACERCA DE");
		btnAcercaDe.setBounds(312, 11, 122, 23);
		frmMecanografia.getContentPane().add(btnAcercaDe);
		
		btnAcercaDe.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				JOptionPane.showMessageDialog(null, "Version 1.0\nCreador: Javier Elvira L�pez\nRelease date: 15/10/2019","ACERCA DE", JOptionPane.INFORMATION_MESSAGE);
			}
		});
	}
}
