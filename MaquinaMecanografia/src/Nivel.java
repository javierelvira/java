import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;

public class Nivel {

	Principal principal = new Principal();

	private JFrame frmMecanografia;

	static int level;

	public JFrame getFrame() {
		return frmMecanografia;
	}

	public void setFrame(JFrame frame) {
		this.frmMecanografia = frame;
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Nivel window = new Nivel();
					window.frmMecanografia.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Nivel() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMecanografia = new JFrame();
		frmMecanografia.setTitle("Mecanografia");
		frmMecanografia.setResizable(false);
		frmMecanografia.setIconImage(Toolkit.getDefaultToolkit().getImage("imagenes\\icono.jpg"));
		frmMecanografia.setBounds(100, 100, 450, 300);
		frmMecanografia.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMecanografia.getContentPane().setLayout(null);
		
		
		JButton btnSencillo = new JButton("SENCILLO");
		btnSencillo.setBounds(150, 50, 150, 50);
		frmMecanografia.getContentPane().add(btnSencillo);
		
		btnSencillo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
						principal.getFrame().setVisible(true);
						frmMecanografia.setVisible(false);
						principal.f.setNivel(0);
						principal.textPane.setText(principal.f.cojeTexto());
						Principal.teclado();
			}
		});
		
		JButton btnDificil = new JButton("DIFICIL");
		btnDificil.setBounds(150, 150, 150, 50);
		frmMecanografia.getContentPane().add(btnDificil);
		
		btnDificil.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

						principal.getFrame().setVisible(true);
						frmMecanografia.setVisible(false);
						principal.f.setNivel(1);
						principal.textPane.setText(principal.f.cojeTexto());
						principal.teclado2();
			}
		});
		
		JButton btnVolver = new JButton("VOLVER");
		btnVolver.setBounds(335, 227, 89, 23);
		frmMecanografia.getContentPane().add(btnVolver);
		
		btnVolver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				Login.stats.getFrame().setVisible(true);
				frmMecanografia.setVisible(false);
			}
		});
	}
}
