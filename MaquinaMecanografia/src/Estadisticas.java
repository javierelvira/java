import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JButton;

public class Estadisticas {

	private JFrame frmMecanografia;
	
	public JFrame getFrame() {
		return frmMecanografia;
	}

	public void setFrame(JFrame frame) {
		this.frmMecanografia = frame;
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Estadisticas window = new Estadisticas();
					window.frmMecanografia.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Estadisticas() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMecanografia = new JFrame();
		frmMecanografia.setTitle("Mecanografia");
		frmMecanografia.setResizable(false);
		frmMecanografia.setIconImage(Toolkit.getDefaultToolkit().getImage("imagenes\\icono.jpg"));
		frmMecanografia.setBounds(100, 100, 450, 300);
		frmMecanografia.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMecanografia.getContentPane().setLayout(null);
		
		JTextArea textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setBounds(50, 50, 325, 150);
		frmMecanografia.getContentPane().add(textArea);
		
		String codigo = new String(), path = "archivos\\stats.txt";
		File archivo = new File(path);

		FileReader fr = null;
		BufferedReader entrada = null;
		try {
			fr = new FileReader(path);
			entrada = new BufferedReader(fr);

			while(entrada.ready()){
				codigo += entrada.readLine();
			}

		}catch(IOException e) {
			e.printStackTrace();
		}finally{
			try{                    
				if(null != fr){   
					fr.close();     
				}                  
			}catch (Exception e2){ 
				e2.printStackTrace();
			}
		}
		textArea.setText(codigo);
		
		JButton btnVolver = new JButton("VOLVER");
		btnVolver.setBounds(335, 227, 89, 23);
		frmMecanografia.getContentPane().add(btnVolver);
		
		btnVolver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				Login.stats.getFrame().setVisible(true);
				frmMecanografia.setVisible(false);
			}
		});
	}

}
