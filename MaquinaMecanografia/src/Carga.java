import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

public class Carga {

	private static JFrame frmMecanografia;
	static Login login1 = new Login();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Carga window = new Carga();
					window.frmMecanografia.setVisible(true);
					verificaFicheros();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		try {
            Thread.sleep(5000);
         } catch (Exception e) {
         }
	 login1.getFrame().setVisible(true);
		frmMecanografia.setVisible(false);
	}

	/**
	 * Create the application.
	 */
	public Carga() {
		initialize();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMecanografia = new JFrame();
		frmMecanografia.setResizable(false);
		frmMecanografia.setTitle("Mecanografia");
		frmMecanografia.setIconImage(Toolkit.getDefaultToolkit().getImage("imagenes\\icono.jpg"));
		frmMecanografia.setBounds(100, 100, 1400, 800);
		frmMecanografia.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMecanografia.getContentPane().setLayout(null);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setBounds(400, 550, 600, 60);
		frmMecanografia.getContentPane().add(progressBar);
		progressBar.setIndeterminate(true);
		
		JLabel imagen = new JLabel("New label");
		imagen.setIcon(new ImageIcon("imagenes\\logo.jpg"));
		imagen.setBounds(0, 0, 1394, 771);
		frmMecanografia.getContentPane().add(imagen);
	}
	
	static void verificaFicheros(){
		
		String rutaUsuarios = ("archivos\\usuarios.txt");
		File fUsuarios = new File(rutaUsuarios);
		if(fUsuarios.exists()) {
		}
		else {
			JOptionPane.showMessageDialog(null, "No se ha encontrado el archivo usuarios","ERROR", JOptionPane.WARNING_MESSAGE);
			System.exit(0);
		}
		
		String rutaStats = ("archivos\\stats.txt");
		File fStats = new File(rutaStats);
		if(fStats.exists()) {
		}
		else {
			JOptionPane.showMessageDialog(null, "No se ha encontrado el archivo Estadisticas","ERROR", JOptionPane.WARNING_MESSAGE);
			System.exit(0);
		}
		          
		String rutaLeccion = ("archivos\\leccion.txt");
		File fLeccion = new File(rutaLeccion);
		if(fLeccion.exists()) {
		}
		else {
			JOptionPane.showMessageDialog(null, "No se ha encontrado el archivo Leccion","ERROR", JOptionPane.WARNING_MESSAGE);
			System.exit(0);
		}
	}
}
