import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;


public class Temporizador{
	

	 private Timer timer = new Timer(); 
	    private static int segundos=0;

	    //Clase interna que funciona como contador
	    class Contador extends TimerTask {
	        public void run() {
	            segundos++;
	            Principal.correTiempo();
	        }
	    }
	    //Crea un timer, inicia segundos a 0 y comienza a contar
	    public void Contar()
	    {
	        this.segundos=0;
	        timer = new Timer();
	        timer.schedule(new Contador(), 0, 1000);
	    }
	    //Detiene el contador
	    public void Detener() {
	        timer.cancel();
	    }
	    //Metodo que retorna los segundos transcurridos
	    public static int getSegundos()
	    {
	        return segundos;
	    }
	    
	    
	
}
