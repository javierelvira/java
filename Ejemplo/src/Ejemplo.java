import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ejemplo {

	private JFrame frmEjemplo;

	public JFrame getFrmEjemplo() {
		return frmEjemplo;
	}

	public void setFrmEjemplo(JFrame frmEjemplo) {
		this.frmEjemplo = frmEjemplo;
	}

	hija hija = new hija(this);

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejemplo window = new Ejemplo();
					window.frmEjemplo.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejemplo() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmEjemplo = new JFrame();
		frmEjemplo.setTitle("Principal");
		frmEjemplo.setBounds(100, 100, 740, 454);
		frmEjemplo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmEjemplo.getContentPane().setLayout(null);

		JButton btnNewButton = new JButton("Ir a Hija >>");
		btnNewButton.setFont(new Font("Verdana", Font.BOLD, 20));
		btnNewButton.setBounds(213, 153, 263, 74);
		frmEjemplo.getContentPane().add(btnNewButton);

		// Eventos
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				hija.getFrmHija().setVisible(true);
				frmEjemplo.setVisible(false);
			}
		});
		
	}

}
