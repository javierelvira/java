﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pizzeria_JavierElvira
{
    public partial class Form1 : Form
    {
        private int precio;
        private double total;
        private bool comp1 = false;
        private bool comp2 = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkedListBoxPrincipal.Enabled = false;
            foreach (int checkedItemIndex in checkedListBoxPrincipal.CheckedIndices) { checkedListBoxPrincipal.SetItemChecked(checkedItemIndex, false); }

            if (comboBox1.SelectedIndex == 0)
            {
                checkedListBoxPrincipal.SetItemChecked(0, true);
                checkedListBoxPrincipal.SetItemChecked(1, true);
                comboBox2.Enabled = true;
                comboBox3.Enabled = true;
            }
            if (comboBox1.SelectedIndex == 1)
            {

                comboBox2.SelectedIndex = 2;
                comboBox2.Enabled = false;
                checkedListBoxPrincipal.SetItemChecked(2, true);
                checkedListBoxPrincipal.SetItemChecked(3, true);
                comboBox3.Enabled = true;
                checkBox1.Enabled = false;
                checkBox4.Enabled = false;
                checkBox5.Enabled = false;
                checkBox6.Enabled = false;
            }
            if (comboBox1.SelectedIndex == 2)
            {
                checkedListBoxPrincipal.SetItemChecked(4, true);
                checkedListBoxPrincipal.SetItemChecked(5, true);
                comboBox2.Enabled = true;
                comboBox3.Enabled = true;
            }
            if (comboBox1.SelectedIndex == 3)
            {
                checkedListBoxPrincipal.SetItemChecked(6, true);
                checkedListBoxPrincipal.SetItemChecked(7, true);
                checkedListBoxPrincipal.SetItemChecked(8, true);
                checkedListBoxPrincipal.SetItemChecked(9, true);
                comboBox3.SelectedIndex = 0;
                comboBox3.Enabled = false;
                comboBox2.Enabled = true;
                checkBox1.Enabled = false;
                checkBox2.Enabled = false;
                checkBox3.Enabled = false;
                checkBox4.Enabled = false;
                checkBox5.Enabled = false;
                checkBox6.Enabled = false;
            }
        }

        private void listViewResumen_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox2.SelectedIndex == 0)
            {
                comp1 = true;
            }
            else if (comboBox2.SelectedIndex == 1)
            {
                comp1 = true;
            }
            else if (comboBox2.SelectedIndex == 2)
            {
                comp1 = true;
            }
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox3.SelectedIndex == 0)
            {
                comp2 = true;
            }
            else if (comboBox3.SelectedIndex == 1)
            {
                comp2 = true;
            }
            else if (comboBox3.SelectedIndex == 2)
            {
                comp2 = true;
            }
            else if (comboBox3.SelectedIndex == 3)
            {
                comp2 = true;
            }
        }

        private void buttonResumen_Click(object sender, EventArgs e)
        {
            total = 0;
            int cnt = 0;
            if (checkBox1.Checked)
            {
                cnt += 1;
            }
            if (checkBox2.Checked)
            {
                cnt += 1;
            }
            if (checkBox3.Checked)
            {
                cnt += 1;
            }
            if (checkBox4.Checked)
            {
                cnt += 1;
            }
            if (checkBox5.Checked)
            {
                cnt += 1;
            }
            if (checkBox6.Checked)
            {
                cnt += 1;
            }
            
            if ((cnt >= 2) && (cnt <= 4) && (comp1 == true) && (comp2 == true))
            {
                buttonPrecio.Enabled = true;

                listViewResumen.Items.Clear();

                ListViewGroup tipoPizza = new ListViewGroup("Pizza:", HorizontalAlignment.Center);
                ListViewGroup ingredientesPizza = new ListViewGroup("Ingredientes Principales:", HorizontalAlignment.Center);
                ListViewGroup ingredientesPizza2 = new ListViewGroup("Ingredientes Secundarios:", HorizontalAlignment.Center);
                ListViewGroup tamañoPizza = new ListViewGroup("Tamaño:", HorizontalAlignment.Center);
                ListViewGroup masaPizza = new ListViewGroup("Masa:", HorizontalAlignment.Center);

                listViewResumen.Items.Add(new ListViewItem(comboBox1.SelectedItem + "", tipoPizza));
                for (int cnt2 = 0; cnt2 < checkedListBoxPrincipal.CheckedItems.Count; cnt2++)
                {
                    listViewResumen.Items.Add(new ListViewItem(checkedListBoxPrincipal.CheckedItems[cnt2] + "", ingredientesPizza));
                }

                if (checkBox1.Checked)
                {
                    listViewResumen.Items.Add(new ListViewItem(checkBox1.Text + "", ingredientesPizza2));
                }
                if (checkBox2.Checked)
                {
                    listViewResumen.Items.Add(new ListViewItem(checkBox2.Text + "", ingredientesPizza2));
                }
                if (checkBox3.Checked)
                {
                    listViewResumen.Items.Add(new ListViewItem(checkBox3.Text + "", ingredientesPizza2));
                }
                if (checkBox4.Checked)
                {
                    listViewResumen.Items.Add(new ListViewItem(checkBox4.Text + "", ingredientesPizza2));
                }
                if (checkBox5.Checked)
                {
                    listViewResumen.Items.Add(new ListViewItem(checkBox5.Text + "", ingredientesPizza2));
                }
                if (checkBox6.Checked)
                {
                    listViewResumen.Items.Add(new ListViewItem(checkBox6.Text + "", ingredientesPizza2));
                }
                listViewResumen.Items.Add(new ListViewItem(comboBox2.SelectedItem + "", tamañoPizza));
                listViewResumen.Items.Add(new ListViewItem(comboBox3.SelectedItem + "", masaPizza));

                //GRUPOS
                listViewResumen.Groups.Add(tipoPizza);
                listViewResumen.Groups.Add(ingredientesPizza);
                listViewResumen.Groups.Add(ingredientesPizza2);
                listViewResumen.Groups.Add(tamañoPizza);
                listViewResumen.Groups.Add(masaPizza);
            //    buttonResumen.Enabled = false;

                if (comboBox1.SelectedIndex == 0)
                {
                    total += 2.95;
                }
                else if (comboBox1.SelectedIndex == 1)
                {
                    total += 5;
                }
                else if (comboBox1.SelectedIndex == 2)
                {
                    total += 3.5;
                }
                else if (comboBox1.SelectedIndex == 3)
                {
                    total += 3;
                }
                if (comboBox3.SelectedIndex == 0)
                {
                    total += 0.5;
                }
                else if (comboBox3.SelectedIndex == 1)
                {
                    total += 1;
                }
                else if (comboBox3.SelectedIndex == 2)
                {
                    total += 1.5;
                }
                else if (comboBox3.SelectedIndex == 3)
                {
                    total += 3;
                }
                if (comboBox2.SelectedIndex == 0)
                {
                    total += 12;
                }
                else if (comboBox2.SelectedIndex == 1)
                {
                    total += 9;
                }
                else if (comboBox2.SelectedIndex == 2)
                {
                    total += 6.5;
                }

                if (checkBox1.Checked)
                {
                    total += 1;
                }
                else if (checkBox2.Checked)
                {
                    total += 1;
                }
                else if (checkBox3.Checked)
                {
                    total += 1;
                }
                else if (checkBox4.Checked)
                {
                    total += 1;
                }
                else if (checkBox5.Checked)
                {
                    total += 1;
                }
                else if (checkBox6.Checked)
                {
                    total += 1;
                }
            }
            else
            {
                string mensage = "Debe rellenar todos los campos y tener entre 2 y 4 ingredientes secundarios";
                string titulo = "Error en datos";
                MessageBoxButtons opciones = MessageBoxButtons.OK;
                DialogResult result = MessageBox.Show(mensage, titulo, opciones,
                MessageBoxIcon.Error);
            }
        }

        private void buttonPrecio_Click(object sender, EventArgs e)
        {
            
            string mensage = "El importe de su pedido es: " + total + "€";
            string titulo = "Factura";
            MessageBoxButtons opciones = MessageBoxButtons.OK;
            DialogResult result = MessageBox.Show(mensage, titulo, opciones,
            MessageBoxIcon.Information);
        }

        private void buttonSalir_Click(object sender, EventArgs e)
        {
            string mensage = "¿Realmente desea salir ?";
            string titulo = "Salir";
            MessageBoxButtons opciones = MessageBoxButtons.YesNo;
            DialogResult result = MessageBox.Show(mensage, titulo, opciones,
            MessageBoxIcon.Question);
            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                Application.Exit();
            }
        }
    }
}
